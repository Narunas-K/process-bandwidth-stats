#!/usr/bin/python
## Version 2.0.0
## By Narunas Kapocius
## Last edited 20190509

import socket, sys
from  struct import *
import time
import json
import os
import subprocess
import re
import multiprocessing
from collections import deque
import numpy as np
import matplotlib.pyplot as plt
from scapy.all import sniff
from multiprocessing import Process, Manager, Value


class CircularBuffer(deque):
    def __init__(self, size=0):
        super(CircularBuffer, self).__init__(maxlen=size)
    @property
    def average(self):
        return sum(self)/len(self)


def readSocketFile ():
    fullSocketList = []
    try:
        with open('/proc/net/tcp','r') as f:
                    for line in f:
                        line = line.split(" ")
                        line = list(filter (None, line))
                        if line[0] != "sl":
                            rawSrcAddrSrcPort = line[1].split(":")
                            rawSrcAddr = rawSrcAddrSrcPort[0]
                            rawSrcPort = rawSrcAddrSrcPort[1]
                            decSrcAddr = str(int(rawSrcAddr[6]+rawSrcAddr[7], 16)) + "." + str(int(rawSrcAddr[4] + rawSrcAddr[5],16)) + "." + str(int(rawSrcAddr[2] + rawSrcAddr[3], 16)) + "." + str(int(rawSrcAddr[0] +rawSrcAddr[1], 16))
                            rawDstAddrDstPort = line[2].split(":")
                            rawDstAddr = rawDstAddrDstPort[0]
                            rawDstPort = rawDstAddrDstPort[1]
                            decDstAddr = str(int(rawDstAddr[6]+rawDstAddr[7], 16)) + "." + str(int(rawDstAddr[4] + rawDstAddr[5],16)) + "." + str(int(rawDstAddr[2] + rawDstAddr[3], 16)) + "." + str(int(rawDstAddr[0] +rawDstAddr[1], 16))
                            decSrcPort = str(int("0x"+rawSrcPort, 16))
                            decDstPort = str(int("0x"+rawDstPort, 16))
                            inode = str(line[9])
                            socketList = (decSrcAddr, decSrcPort, decDstAddr, decDstPort, inode)
                            fullSocketList.append(socketList)
    except:
        print ("Process is closed. Exiting..")
        exit()
    return fullSocketList


def returnActiveSockets(pid):
    fullSocketList = readSocketFile()
    try:
        lsCommand = "ls -ltrh /proc/" + pid + "/fd"
        process = subprocess.Popen(lsCommand.split(), stdout = subprocess.PIPE)
        lsOutput, error = process.communicate()
        lsOutput = str(lsOutput).split("\\n")
        if (lsOutput[0] != "b''"):
            inodeList = []
            for item in lsOutput:
                searchObj = re.search( r'(socket:\[\d{1,})', item, re.M|re.I)
                if searchObj:
                    inodeList.append(str(searchObj.group(1)).split("[")[1])
            activeSocketList = []
            for item in fullSocketList:
                for inode in inodeList:
                    if str(inode) in (item[4]):
                        activeSocketList.append(item)
            return activeSocketList
        else:
            print ("Processis closed. Exiting..")
            exit()
    except:
        print ("Processis closed. Exiting..")
        exit()


def output():
    global downloadSpeed, uploadSpeed, cb
    try:
        plt.plot(cb, list(downloadSpeed), label="Download speed")
        plt.plot(cb, list(uploadSpeed), label = "Upload speed")
        plt.draw()
        plt.legend(loc='upper right')
        plt.ylabel("Bandwidth MB/s")
        plt.xlabel("time, s")
        plt.pause(0.000001)
        plt.clf()
    except:
        print ("Error in ploting data. Exiting..")
        exit()       


def buildQuery (activeSocketsList):
    query = ""
    count = 1
    listLen = len(activeSocketsList)
    print (listLen)
    for socket in activeSocketsList:  
        if listLen == 1:
            query =  "(" + socket[1] + " or " + socket[3] + ")"
        elif count == 1:
            query =  "(" + socket[1] + " or " + socket[3]
        elif count == listLen:
            query =query + " or " +socket[1] + " or " + socket[3] + ")"
        else:
            query = query + " or " +socket[1] + " or " + socket[3]
        count = count + 1
    query = "tcp port " + query 
    return query


def sniffTraffic():
    global query
    query = buildQuery (activeSockets)
    sniff(filter=query, prn=dataCalculation)


def dataCalculation(packet):
    global activeSockets, uploadBytes, downloadBytes
    for item in activeSockets:
        if str(item[0]) == str(packet[0][1].dst):
            downloadBytes[0] = downloadBytes[0] + int(len(packet))
        elif str(item[2]) == str(packet[0][1].dst):
            uploadBytes[0] = uploadBytes[0] + int(len(packet))

def visualisation():
    global uploadBytes, downloadBytes, cb, downloadSpeed, uploadSpeed
    startTime = time.time()
    count = 0
    while True:
        if time.time() - startTime >1:
            startTime = time.time()
            count = count +1  
            cb.append(count)
            downloadSpeed.append(downloadBytes[0]/1000000)
            uploadSpeed.append(uploadBytes[0]/1000000)
            print ("Download: " + str(downloadSpeed))
            print ("Upload: " + str(uploadSpeed))
            output ()
            uploadBytes[0] = 0
            downloadBytes[0] = 0

def updateSockets():
	global pid, activeSockets
	start = time.time()
	while True:
		if time.time() - start >5: 
			activeSockets = returnActiveSockets (pid) 
			start = time.time()
			print ("Updating sockets list")


if __name__ == '__main__':
    pid = input ("Enter process ID (PID):")
    size = int(input ("Enter Y axis lentgh (s): "))
    cb = CircularBuffer(size)
    downloadSpeed = CircularBuffer(size)
    uploadSpeed = CircularBuffer(size)
    activeSockets = returnActiveSockets (pid) 
    query = buildQuery (activeSockets)
    print ("Scapy query: " + query)
    manager = multiprocessing.Manager()
    downloadBytes = manager.list()
    uploadBytes = manager.list()
    downloadBytes.append(0)
    uploadBytes.append(0)
    print ("Starting to measure..")
    
    process1 = multiprocessing.Process(target=sniffTraffic)
    process2 = multiprocessing.Process(target=visualisation)
    #process3 = multiprocessing.Process(target=updateSockets)
    process1.start()
    process2.start()
    #process3.start()
    process1.join()
    process2.join()



